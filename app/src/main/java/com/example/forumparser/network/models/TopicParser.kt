package com.example.forumparser.network.models

import com.example.forumparser.data.Topic
import org.jsoup.Jsoup
import org.jsoup.select.Elements

object TopicParser {
    fun getTopicList(link: String): List<Topic> {
        try {
            val topics = ArrayList<Topic>()
            val docTopic = Jsoup.connect(BASE_URL + link).get()
            val topicsList: Elements = docTopic.select("li.row dl.icon")
            for (topic in topicsList) {

                val topicLink = topic.select("a[href].topictitle").first() ?: continue
                val linkHref = topicLink.attr("href")
                val linkText = topicLink.text()
                if (topic.select("dd.posts").first() == null) continue
                val posts = topic.select("dd.posts").first().text().split("\\s".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0]
                val views = topic.select("dd.views").first().text().split("\\s".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0]

                topics.add(Topic(linkText, BASE_URL + linkHref, posts, views))
            }
            return topics
        } catch (_: Throwable) {
            return listOf()
        }

    }
}