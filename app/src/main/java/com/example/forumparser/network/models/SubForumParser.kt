package com.example.forumparser.network.models

import com.example.forumparser.data.SubForum
import org.jsoup.Jsoup
import org.jsoup.select.Elements

object SubForumParser {
    fun getSubforumsList(link: String): List<SubForum> {

        try {
            val subForums = mutableListOf<SubForum>()
            val subDocument = Jsoup.connect(BASE_URL + link).get()
            val topicsList: Elements = subDocument.select("li.row dl.icon")

            for (topic in topicsList) {
                val topicLink = topic.select("a[href].forumtitle").first() ?: continue
                val linkText = topicLink.text()
                if (topic.select("span.datetime").first() == null) continue
                val postsCount = topic.select("dd.posts").first().text().split("\\s".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0]
                val topicsCount = topic.select("dd.topics").text().split("\\s".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()[0]

                subForums.add(SubForum(
                        linkText,
                        topicLink.attr("href").toString(),
                        postsCount,
                        topicsCount
                    )
                )
            }
            return subForums
        } catch (_: Throwable) {
            return listOf()
        }
    }
}