package com.example.forumparser.network.models

import android.util.Log
import com.example.forumparser.data.Post
import org.jsoup.Jsoup
import org.jsoup.select.Elements

object PostParser {

    fun getPostList(link: String): List<Post> {
        val posts = ArrayList<Post>()
        val docPosts = Jsoup.connect(link).get()
        val postList: Elements = docPosts.select("div.post")
        for (post in postList) {
            var top = false
            if (post.hasClass("bg5")) {
                top = true
            }
            val header: String? = if (post.select("h3").first() != null) {
                post.select("h3").first().text()
            } else {
                ""
            }
            val author = post.select("p.author strong").text()
            if (author == "") continue
            var dateTime = post.select("p.author").text()
            dateTime = dateTime.substring(author.length + 2)
            val postBody = post.select("div.content").first()
            val googleAds = postBody.select("#ads-10").first()
            if (googleAds != null) {
                Log.d("whoa", "remove kebab")
                googleAds.remove()
                //postBody.children().remove(googleAds);
            }
            val content = postBody.text()

            posts.add(Post(header, author, content, top, 0))

        }
        Log.d("elements size", postList.size.toString() + " to " + posts.size)
        return posts
    }

}