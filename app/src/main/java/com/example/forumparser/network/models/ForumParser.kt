package com.example.forumparser.network.models

import android.util.Log
import com.example.forumparser.data.Forum
import org.jsoup.Jsoup
import org.jsoup.select.Elements

const val BASE_URL = "https://forum.awd.ru/"

object ForumParser {

    fun getForumsList(): List<Forum> {
        val forums = ArrayList<Forum>()
        val doc = Jsoup.connect(BASE_URL).get()
        val topicsList: Elements = doc.select("div.forabg")
            .select("div.inner")
            .select("ul.topiclist")
            .select("li.header")
            .select("dl")
            .select("dt")
            .select("a[href]")
        for (link in topicsList) {
            val linkHref = link.attr("href")
            if (linkHref.substring(0, 1) != ".") continue
            val linkText = link.text()

            forums.add(Forum(linkText, linkHref))
            Log.d("element", "$linkHref - $linkText")
        }
        Log.d("elements size", topicsList.size.toString() + " to " + forums.size)
        return forums
    }
}