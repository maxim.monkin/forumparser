package com.example.forumparser.fragments.parseforums

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.Forum
import com.example.forumparser.databinding.ForumItemBinding

class ForumAdapter(var data: List<Forum>, private val clickListener: ForumClickInteractor) :
    RecyclerView.Adapter<ForumViewHolder>() {

    fun updateAdapter(newData: List<Forum>) {
        val difUtil =
            TopicsDifUtilCallback(data, newData)
        val difUtilResult = DiffUtil.calculateDiff(difUtil)
        data = newData
        difUtilResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForumViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ForumItemBinding.inflate(inflater, parent, false)

        return ForumViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ForumViewHolder, position: Int) {

        holder.bind(data[position], clickListener)
    }

    class TopicsDifUtilCallback(
        private val oldList: List<Forum>,
        private val newList: List<Forum>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].link == newList[newItemPosition].link
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }
}