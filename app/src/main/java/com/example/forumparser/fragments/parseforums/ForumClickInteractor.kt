package com.example.forumparser.fragments.parseforums

import com.example.forumparser.data.Forum

interface ForumClickInteractor {
    fun onTopicClicked(forum: Forum)
}