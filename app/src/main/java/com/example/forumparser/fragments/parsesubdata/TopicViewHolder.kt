package com.example.forumparser.fragments.parsesubdata

import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.Topic
import com.example.forumparser.databinding.TopicItemBinding

class TopicViewHolder(private val binding: TopicItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(topic: Topic, interactor: SubDataClickInteractor) {
        binding.subMainTitle.text = topic.title
        binding.numberOfThemes.text = topic.titleComments
        binding.numberOfMessages.text = topic.titleViews
        binding.root.setOnClickListener {
            interactor.onTopicClicked(topic)
        }
    }
}