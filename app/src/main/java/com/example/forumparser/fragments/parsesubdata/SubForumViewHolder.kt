package com.example.forumparser.fragments.parsesubdata

import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.SubForum
import com.example.forumparser.databinding.SubforumItemBinding

class SubForumViewHolder(private val binding: SubforumItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(subForum: SubForum, interactor: SubDataClickInteractor) {
        binding.subMainTitle.text = subForum.subTitle
        binding.numberOfThemes.text = subForum.theme
        binding.numberOfMessages.text = subForum.message
        binding.root.setOnClickListener {
            interactor.onSubForumClicked(subForum)
        }
    }
}