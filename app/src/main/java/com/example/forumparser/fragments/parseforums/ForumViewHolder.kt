package com.example.forumparser.fragments.parseforums

import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.Forum
import com.example.forumparser.databinding.ForumItemBinding
import com.example.forumparser.databinding.TopicItemBinding

class ForumViewHolder(val binding: ForumItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(forum: Forum, clickListener: ForumClickInteractor) {
        binding.mainTitle.text = forum.title
        binding.root.setOnClickListener {
            clickListener.onTopicClicked(forum)
        }
    }
}