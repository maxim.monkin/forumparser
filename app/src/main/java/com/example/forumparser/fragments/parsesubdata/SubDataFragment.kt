package com.example.forumparser.fragments.parsesubdata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.forumparser.R
import com.example.forumparser.data.SubData
import com.example.forumparser.data.SubForum
import com.example.forumparser.data.Topic
import com.example.forumparser.databinding.SubdataFragmentBinding
import com.example.forumparser.factory
import com.example.forumparser.fragments.parsepost.PostFragment

class SubDataFragment : Fragment(R.layout.subdata_fragment), SubDataClickInteractor {

    private lateinit var binding: SubdataFragmentBinding
    private val viewModel: SubDataViewModel by viewModels { factory() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SubdataFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(POSTDATA)?.let {
            viewModel.initiliaze(it)
        }

        viewModel.subData.observe(viewLifecycleOwner) {
            updateAdapter(it)
        }
    }

    fun updateAdapter(subData: List<SubData>) {
        if (binding.subRecyclerView.adapter == null) {
            binding.subRecyclerView.adapter = SubDataAdapter(subData, this)
        } else {
            (binding.subRecyclerView.adapter as SubDataAdapter).updateSubDataList(subData)
        }
    }

    override fun onTopicClicked(topic: Topic) {
        //Toast.makeText(requireContext(),  "subTopic", Toast.LENGTH_SHORT).show()
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.activity_main, PostFragment.newPostInstance(topic))
            .addToBackStack("TopicScreen")
            .commit()
    }

    override fun onSubForumClicked(subForum: SubForum) {
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.activity_main, newPostInstance(subForum.subLink))
            .addToBackStack("SubTopicScreen")
            .commit()
    }

    companion object {
        const val POSTDATA = "postdata"
        fun newPostInstance(link: String): SubDataFragment {
            val fragment = SubDataFragment()
            val bundle = Bundle().apply { putString(POSTDATA, link) }
            fragment.arguments = bundle
            return fragment
        }


    }


}