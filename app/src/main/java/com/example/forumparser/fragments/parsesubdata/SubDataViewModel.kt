package com.example.forumparser.fragments.parsesubdata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.forumparser.data.SubData
import com.example.forumparser.data.SubForum
import com.example.forumparser.data.Topic
import com.example.forumparser.network.models.SubForumParser
import com.example.forumparser.network.models.TopicParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SubDataViewModel : ViewModel() {

    private val _subForum = MutableLiveData<List<SubForum>>()
    val subForum: LiveData<List<SubForum>> get() = _subForum

    private val _topics = MutableLiveData<List<Topic>>()
    val topic: LiveData<List<Topic>> get() = _topics

    private val _subData = MutableLiveData<List<SubData>>()
    val subData: LiveData<List<SubData>> get() = _subData

    fun initiliaze(link: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = SubForumParser.getSubforumsList(link)
            val newList = subData.value?.let { ArrayList(it) }?: ArrayList()
            newList.addAll(response)
            _subData.postValue(newList)
        }
        viewModelScope.launch(Dispatchers.IO) {
            val response = TopicParser.getTopicList(link)
            val newList = subData.value?.let { ArrayList(it) }?: ArrayList()
            newList.addAll(response)
            _subData.postValue(newList)
        }
    }

}