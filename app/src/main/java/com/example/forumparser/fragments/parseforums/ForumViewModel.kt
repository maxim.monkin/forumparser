package com.example.forumparser.fragments.parseforums

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.forumparser.data.Forum
import com.example.forumparser.network.models.ForumParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ForumViewModel() : ViewModel() {

    private val _topics = MutableLiveData<List<Forum>>()
    val topics: LiveData<List<Forum>> get() = _topics

    init {
        initializeInfo()
    }

    private fun initializeInfo() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = ForumParser.getForumsList()
            _topics.postValue(response)
        }
    }
}