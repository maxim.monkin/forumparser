package com.example.forumparser.fragments.parseforums

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.forumparser.R
import com.example.forumparser.data.Forum
import com.example.forumparser.databinding.ForumsFragmentBinding
import com.example.forumparser.factory
import com.example.forumparser.fragments.parsesubdata.SubDataFragment

class ForumFragment : Fragment(R.layout.forums_fragment), ForumClickInteractor {

    private lateinit var binding: ForumsFragmentBinding

    private val viewModel: ForumViewModel by viewModels { factory() }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = ForumsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.topics.observe(viewLifecycleOwner) { topics ->
            if (binding.recyclerView.adapter == null) {
                binding.recyclerView.adapter = ForumAdapter(topics, this)


            } else {
                (binding.recyclerView.adapter as ForumAdapter).updateAdapter(topics)
            }
        }
    }

    override fun onTopicClicked(forum: Forum) {
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.activity_main, SubDataFragment.newPostInstance(forum.link))
            .addToBackStack("TopicScreen")
            .commit()
    }
}