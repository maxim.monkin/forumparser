package com.example.forumparser.fragments.parsesubdata

import com.example.forumparser.data.SubForum
import com.example.forumparser.data.Topic

interface SubDataClickInteractor {
    fun onTopicClicked(topic: Topic)
    fun onSubForumClicked(subForum: SubForum)
}