package com.example.forumparser.fragments.parsesubdata

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.SubData
import com.example.forumparser.data.SubForum
import com.example.forumparser.data.Topic
import com.example.forumparser.databinding.SubforumItemBinding
import com.example.forumparser.databinding.TopicItemBinding

class SubDataAdapter(
    var dataList: List<SubData>,
    val interactor: SubDataClickInteractor
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = SubforumItemBinding.inflate(inflater, parent, false)
                SubForumViewHolder(binding)
            }

            else -> {
                val inflater = LayoutInflater.from(parent.context)
                val binding = TopicItemBinding.inflate(inflater, parent, false)
                TopicViewHolder(binding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position] is SubForum) {
            0
        } else {
            1
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SubForumViewHolder -> {
                holder.bind(dataList[position] as SubForum, interactor)
            }

            is TopicViewHolder -> {
                holder.bind(dataList[position] as Topic, interactor)
            }
        }
    }

    fun updateSubDataList(newSubData: List<SubData>) {
        val diffCallback = SubDataDiffUtilCallback(ArrayList(dataList), ArrayList(newSubData))
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        dataList = newSubData
        diffResult.dispatchUpdatesTo(this)
    }

    private class SubDataDiffUtilCallback(
        oldList: ArrayList<SubData>,
        newList: ArrayList<SubData>
    ) : DiffUtil.Callback() {
        private var oldContactList: ArrayList<SubData> = oldList
        private var newContactList: ArrayList<SubData> = newList

        override fun getOldListSize(): Int {
            return oldContactList.size
        }

        override fun getNewListSize(): Int {
            return newContactList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldContactList[oldItemPosition]
            val newItem = newContactList[newItemPosition]

            return if (oldItem is SubForum && newItem is SubForum) {
                (oldContactList[oldItemPosition] as SubForum).subLink == (newContactList[newItemPosition] as SubForum).subLink
            } else if (oldItem is Topic && newItem is Topic) {
                (oldContactList[oldItemPosition] as Topic).titleLink == (newContactList[newItemPosition] as Topic).titleLink
            } else {
                false
            }
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldContactList[oldItemPosition]
            val newItem = newContactList[newItemPosition]

            return if (oldItem is SubForum && newItem is SubForum) {
                (oldContactList[oldItemPosition] as SubForum) == (newContactList[newItemPosition] as SubForum)
            } else if (oldItem is Topic && newItem is Topic) {
                (oldContactList[oldItemPosition] as Topic) == (newContactList[newItemPosition] as Topic)
            } else {
                false
            }
        }
    }

}