package com.example.forumparser.fragments.parsepost

import androidx.recyclerview.widget.RecyclerView
import com.example.forumparser.data.Post
import com.example.forumparser.databinding.PostItemBinding

class PostViewHolder(private val binding: PostItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
        fun bind(post: Post){
            binding.itemTextAuthor.text = post.author
            binding.itemTextContent.text = post.content
            binding.itemTextHeader.text = post.header
        }

}