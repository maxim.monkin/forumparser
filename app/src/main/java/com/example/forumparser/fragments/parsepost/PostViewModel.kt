package com.example.forumparser.fragments.parsepost

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.forumparser.data.Forum
import com.example.forumparser.data.Post
import com.example.forumparser.data.Topic
import com.example.forumparser.network.models.PostParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostViewModel(): ViewModel() {
    private val _posts = MutableLiveData<List<Post>>()
    val posts: LiveData<List<Post>> get() = _posts

    fun  initializeInfo(topic: Topic){
        viewModelScope.launch(Dispatchers.IO) {
            val response = PostParser.getPostList(topic.titleLink)
            _posts.postValue(response)
        }
    }
}