package com.example.forumparser.fragments.parsepost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.forumparser.R
import com.example.forumparser.data.Topic
import com.example.forumparser.databinding.PostFragmentBinding
import com.example.forumparser.factory

class PostFragment : Fragment(R.layout.post_fragment) {
    private lateinit var binding: PostFragmentBinding
    private val viewModel: PostViewModel by viewModels {factory()}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PostFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getSerializable(POST)?.let {
            viewModel.initializeInfo(it as Topic)
        }
        viewModel.posts.observe(viewLifecycleOwner){ post ->
            if (binding.postRecyclerView.adapter== null){
                binding.postRecyclerView.adapter = PostAdapter(post)
            }else{
                (binding.postRecyclerView.adapter as PostAdapter).updateAdapter(post)
            }
        }
        parentFragmentManager.restoreBackStack("ForumScreen")
    }


    companion object{
        const val POST = "post"
        fun newPostInstance(topic: Topic): PostFragment{
            val fragment = PostFragment()
            val bundle = Bundle().apply { putSerializable(POST, topic) }
            fragment.arguments = bundle
            return fragment
        }
    }
}