package com.example.forumparser.data

import java.io.Serializable

data class Topic(
    val title: String,
    val titleLink: String,
    val titleComments: String,
    val titleViews: String,
): Serializable, SubData()
