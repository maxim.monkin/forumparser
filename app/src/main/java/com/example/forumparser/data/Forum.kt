package com.example.forumparser.data

import java.io.Serializable

data class Forum(
    val title: String,
    val link: String,
) : Serializable