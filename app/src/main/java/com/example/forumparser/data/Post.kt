package com.example.forumparser.data

import java.io.Serializable

data class Post(
    val header: String?,
    val author: String,
    val content: String,
    val isTop: Boolean,
    val postNum: Int,
): Serializable