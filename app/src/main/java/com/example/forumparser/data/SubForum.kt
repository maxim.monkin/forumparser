package com.example.forumparser.data

import java.io.Serializable

data class SubForum(
    val subTitle: String,
    val subLink: String,
    val theme: String,
    val message: String,
): Serializable, SubData()