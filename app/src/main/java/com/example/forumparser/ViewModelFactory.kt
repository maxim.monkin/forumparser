package com.example.forumparser

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.forumparser.fragments.parseforums.ForumViewModel
import com.example.forumparser.fragments.parsepost.PostViewModel
import com.example.forumparser.fragments.parsesubdata.SubDataViewModel

class ViewModelFactory(
    context: Context
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val viewModel = when (modelClass) {
            ForumViewModel::class.java -> {
                ForumViewModel()
            }

            SubDataViewModel::class.java -> {
                SubDataViewModel()
            }

            PostViewModel::class.java -> {
                PostViewModel()
            }

            else -> {
                throw IllegalAccessException("Unknown view model class")
            }
        }
        return viewModel as T
    }
}

fun Fragment.factory() = ViewModelFactory(requireContext().applicationContext)