package com.example.forumparser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.forumparser.fragments.parseforums.ForumFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.activity_main, ForumFragment())
            .commit()

    }
}